#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(640,480));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_LineBox_currentIndexChanged(int index)
{
    switch (index)
    {
    case 0:
        ui->DirBox->clear();
        ui->StationBox->clear();
        break;
    case 1:
        ui->DirBox->clear();
        ui->StationBox->clear();
        ui->DirBox->addItems(QStringList() << "Depo Hostivař" << "Nemocnice Motol");
        break;
    case 2:
        ui->DirBox->clear();
        ui->StationBox->clear();
        ui->DirBox->addItems(QStringList() << "Zličín" << "Černý most");
        break;
    case 3:
        ui->DirBox->clear();
        ui->StationBox->clear();
        ui->DirBox->addItems(QStringList() << "Letňany" << "Háje");
        break;
    }
}


void MainWindow::on_DirBox_currentTextChanged(const QString &arg1)
{
    if(arg1 == QString("Depo Hostivař"))
    {
        ui->ExitList->setText("");
        ui->StationBox->clear();
        ui->StationBox->addItems(QStringList() << "Petřiny" << "Nádraží Veleslavín" << "Bořislavka" << "Dejvická" << "Hradčanská"
                                 << "Malostranská" << "Staroměstská" << "Můstek" << "Muzeum" << "Náměstí Míru" << "Jiřího z Poděbrad"
                                 << "Flora" << "Želivského" << "Strašnická" << "Skalka" << "Depo Hostivař");
    }
    if(arg1 == QString("Nemocnice Motol"))
    {
        ui->ExitList->setText("");
        ui->StationBox->clear();
        ui->StationBox->addItems(QStringList() << "Skalka" << "Strašnická" << "Želivského" << "Flora" << "Jiřího z Poděbrad"
                                 << "Náměstí Míru" << "Muzeum"  << "Můstek" << "Staroměstská" << "Malostranská" << "Hradčanská" <<
                                  "Dejvická" << "Bořislavka" << "Nádraží Veleslavín" << "Petřiny" << "Nemocnice Motol");
    }
    if(arg1 == QString("Zličín"))
    {
        ui->ExitList->setText("");
        ui->StationBox->clear();
        ui->StationBox->addItems(QStringList() << "Rajská zahrada" << "Hloubětín" << "Kolbenova" << "Vysočanská" << "Českomoravská"
                                 << "Palmovka" << "Invalidovna" << "Křižíkova" << "Florenc" << "Náměstí Republiky" << "Můstek"
                                 << "Národní třída" << "Karlovo náměstí" << "Anděl" << "Smíchovské nádraží" << "Radlická"
                                 << "Jinonice" << "Nové Butovice" << "Hůrka" << "Lužiny" << "Luka" << "Stodůlky" << "Zličín");
    }
    if(arg1 == QString("Černý most"))
    {
        ui->ExitList->setText("");
        ui->StationBox->clear();
        ui->StationBox->addItems(QStringList() << "Stodůlky" << "Luka" << "Lužiny" << "Hůrka" << "Nové Butovice" << "Jinonice"
                                 << "Radlická" << "Smíchovské nádraží" << "Anděl" << "Karlovo náměstí" << "Národní třída"
                                 << "Můstek" << "Náměstí Republiky" << "Florenc" << "Křižíkova" << "Invalidovna" << "Palmovka"
                                 << "Českomoravská" << "Vysočanská" << "Kolbenova" << "Hloubětín" << "Rajská zahrada" << "Černý most");
    }
    if(arg1 == QString("Letňany"))
    {
        ui->ExitList->setText("");
        ui->StationBox->clear();
        ui->StationBox->addItems(QStringList() << "Opatov" << "Chodov" << "Roztyly" << "Kačerov" << "Budějovická" << "Pankrác"
                                 << "Pražského povstání" << "Vyšehrad" << "I. P. Pavlova" << "Muzeum" << "Hlavní nádraží" << "Florenc"
                                 << "Vltavská" << "Nádraží Holešovice" << "Kobylisy" << "Ládví" << "Střížkov" << "Prosek" << "Letňany");
    }
    if(arg1 == QString("Háje"))
    {
        ui->ExitList->setText("");
        ui->StationBox->clear();
        ui->StationBox->addItems(QStringList() << "Prosek" << "Střížkov" << "Ládví" << "Kobylisy" << "Nádraží Holešovice" << "Vltavská"
                                 << "FLorenc" << "Hlavní nádraží" << "Muzeum" << "I. P. Pavlova" << "Vyšehrad" << "Pražského povstání"
                                 << "Pankrác" << "Budějovická" << "Kačerov" << "Roztyly" << "Chodov" << "Opatov" << "Háje");
    }
}
//I am sorry for this implemetation of database. There are about ten thousand ways how to do this task better, but well, I am sorry.

void MainWindow::on_StationBox_currentTextChanged(const QString &arg1)
{
    QString dir = ui->DirBox->currentText();
    if(arg1 == "Nemocnice Motol") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Schody: 1. vůz, 2. dveře"); //START A
    else if(arg1 == "Petřiny")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Schody: 1. vůz, 1. dveře<br>Výtah: 4. vůz, 2. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výtah: 2. vůz, 3. dveře<br>Schody: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Nádraží Veleslavín")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Bořislavka")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Schody: 1. vůz, 1. dveře<br>Výtah: 5. vůz, 3. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výtah: 1. vůz, 2. dveře<br>Schody: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Dejvická")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výtah: 1. vůz, 1 dveře<br>Výstup Dejvická BUS, Vítězné náměstí: 1. vůz, 4. dveře<br>Výstup Dejvická TRAM, Areál vysokých škol: 5. vůz, 4. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup Dejvická TRAM, Areál vysokých škol: 1. vůz, 1. dveře<br>Výstup Dejvická BUS, Vítězné náměstí: 5. vůz, 1. dveře<br> Výtah: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Hradčanská")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 2. vůz, 4. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře");
    }
    else if(arg1 == "Malostranská")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 3. vůz, 2. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup: 3. vůz 3. dveře");
    }
    else if(arg1 == "Staroměstská")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 4. vůz, 4. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup: 2. vůz, 3. dveře");
    }
    else if(arg1 == "Můstek")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup směr Václavské náměstí, Jindřišská: 1. vůz, 1. dveře<br>Přestup linka B: 1. vůz, 2. dveře<br>Výtah na Václavské náměstí: 4. vůz, 3. dveře<br> Přestup na linku B: 5. vůz, 1. dveře<br>Výtah do vestibulu: 5. vůz, 2. dveře<br>Výstup ulice Na příkopě: 5. vůz, 3. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup ulice Na Příkopě: 1. vůz, 1. dveře<br>Výtah do vestibulu: 1. vůz, 2. dveře<br>Přestup na linku B: 1. vůz, 3. dveře<br>Výtah na Václavské náměstí: 2. vůz, 2. dveře<br>Přestup na linku B: 5. vůz, 3. dveře<br>Výstup Václavské náměstí, Jindřišská: 5. vůz, 4. dveře");
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br> Přestup na linku A: 2. vůz, 3. dveře<br>3. vůz, 3. dveře<br>5. vůz, 4. dveře<br>Výtah: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Přestup na linku A: 1. vůz, 1. dveře<br>3. vůz, 2. dveře<br>4. vůz, 2. dveře<br>Výstup: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Muzeum")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výtah: 2. vůz, 2. dveře<br>Východ: 2. vůz, 4. dveře<br>3. vůz, 4. dveře<br>Přestup na linku C: 4. vůz, 3. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Přestup na linku C: 2. vůz, 3. dveře<br>Východ: 3. vůz, 2. dveře<br>Výtah: 4. vůz, 4. dveře");
        if(dir == "Letňany") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Přestup na linku A: 3. vůz, 4. dveře<br>Výstup: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 1. vůz 1. dveře<br>Přestup na linku A: 3. vůz, 1. dveře<br>Výtah: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Náměstí Míru")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup: 2. vůz, 1. dveře");
    }
    else if(arg1 == "Jiřího z Poděbrad")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup: 2. vůz, 1. dveře");
    }
    else if(arg1 == "Flora")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup: 2. vůz, 4. dveře");
    }
    else if(arg1 == "Želivského")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup: 2. vůz, 4. dveře");
    }
    else if(arg1 == "Strašnická")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Skalka")
    {
        if(dir == "Depo Hostivař") ui->ExitList->setText("Výstup: 2. vůz, 2. dveře<br>4. vůz, 1. dveře<br>Výtah: 5. vůz, 4. dveře");
        if(dir == "Nemocnice Motol") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Výstup: 2. vůz, 4. dveře<br>4. vůz, 3. dveře");
    }
    else if(arg1 == "Depo Hostivař") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře"); //END A
    else if(arg1 == "Černý most") ui->ExitList->setText("Výstup 1. vůz, 2. dveře<br>5. vůz, 3. dveře"); //START B
    else if(arg1 == "Rajská zahrada")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br>5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br>5. vůz, 1. dveře");
    }
    else if(arg1 == "Hloubětín")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře<br>Výtah: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Výstup: 2. vůz, 4. dveře");
    }
    else if(arg1 == "Kolbenova")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Výstup: 2. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře<br>Výtah: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Vysočanská")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 1. vůz, 4. dveře<br>2. vůz, 4. dveře<br>Výtah: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Výstup: 4. vůz, 1. dveře<br>5. vůz, 1. dveře");
    }
    else if(arg1 == "Palmovka")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup křižovatka Zenklova/Na Žertvách: 1. vůz, 1. dveře<br>Výstup Na Žertvách, Terminál BUS: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup Na Žertvách, Terminál BUS: 1. vůz, 1. dveře<br>Výstup křižovatka Na Žertvách/Zenklova: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Invalidovna")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 2. vůz, 1. dveře");
    }
    else if(arg1 == "Křižíkova")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 4. vůz, 2. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 2. vůz, 3. dveře");
    }
    else if(arg1 == "Florenc")
    {
        if(dir == "Zličín") ui->ExitList->setText("Přestup na linku C: 2. vůz, 4. dveře<br>Výtah: 3. vůz, 3. dveře<br>Přestup na linku C: 4. vůz, 1. dveře<br>Výstup: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br>Přestup na linku C: 2. vůz, 4. dveře<br>Výtah: 3. vůz, 2. dveře<br>Přestup na linku C: 4. vůz, 1. dveře");
        if(dir == "Letňany") ui->ExitList->setText("Výstup Sokolovská: 1. vůz, 1. dveře<br>Přestup na linku B: 2. vůz, 2. dveře<br>Výstup Křižíkova, CAN Florenc: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup Křižíkova, CAN Florenc: 1. vůz, 1. dveře<br>Přestup na linku B: 4. vůz, 3. dveře<br>Výstup Sokolovská: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Náměstí Republiky")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup Náměstí Republiky, OC Palladium: 1. vůz, 4. dveře<br>Výstup Masarykovo nádraží: 5. vůz, 1. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup Masarykovo nádraží: 1. vůz, 4. dveře<br>Výstup Náměstí Republiky, OC Palladium: 5. vůz, 1. dveře");
    }
    //Můstek under line A
    else if(arg1 == "Národní Třída")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výtah: 2. vůz, 4. dveře<br>Výstup: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br>Výtah: 4. vůz, 1. dveře");
    }
    else if(arg1 == "Karlovo náměstí")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup Palackého náměstí: 1. vůz, 3. dveře<br>Výstup Karlovo náměstí: 5. vůz, 2. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup Karlovo náměstí: 1. vůz, 3. dveře<br>Výstup Palackéhoo náměstí: 5. vůz, 2. dveře");
    }
    else if(arg1 == "Anděl")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup Na Knížecí: 1. vůz, 1. dveře<br>Výtah: 4. vůz, 4. dveře<br>Výstup Anděl: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup Anděl: 1. vůz, 1. dveře<br>Výtah: 2. vůz, 1. dveře<br>Výstup Na Knížecí: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Smíchovské nádraží")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup BUS: 1. vlak, 1. dveře<br>Výstup vlak: 3. vůz, 2. dveře<br>5. vůz, 1. dveře<br>Výstup tramvaj, plošina pro osoby se zdravotním postižením: 5. vůz, 3. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup tramvaj, plošina pro osoby se zdravotním postižením: 1. vůz, 2. dveře<br>Výstup vlak: 1. vůz, 4. dveře<br>3. vůz, 3. dveře<br>Výstup BUS: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Radlická")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Jinonice")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 2. vůz, 4. dveře ");
    }
    else if(arg1 == "Nové Butovice")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup sídliště, plošina pro osoby se zdravotním postižením: 1. vůz, 1. dveře<br>Výstup BUS: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup BUS: 1. vůz, 1. dveře<br>Výstup sídliště, plošina pro osoby se zdravotním postižením: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Hůrka")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Lužiny")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Luka")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Stodůlky")
    {
        if(dir == "Zličín") ui->ExitList->setText("Výstup Západní město: 1. vůz, 1. dveře<br>Výtah: 3. vůz, 1. dveře<br>Výstup sídliště Stodůlky, BUS: 5. vůz, 4. dveře");
        if(dir == "Černý most") ui->ExitList->setText("Výstup sídliště Stodůlky, BUS: 1. vůz, 1. dveře<br>Výtah: 3. vůz, 4. dveře<br>Výstup Západní město: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Zličín") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře"); //END B
    else if(arg1 == "Háje") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br>5. vůz, 4. dveře"); //START C
    else if(arg1 == "Opatov")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Chodov")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Roztyly")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Kačerov")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Budějovická")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Výstup Budějovická: 1. vůz, 2. dveře<br>Výstup Antala Staška: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup Antala Staška: 1. vůz, 1. dveře<br>Výstup Budějovická: 5. vůz, 3. dveře<br>Výtah: 5. vůz, 5. dveře");
    }
    else if(arg1 == "Pankrác")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup: 2. vůz, 2. dveře<br>5. vůz, 1. dveře<br>Výtah: 5. vůz, 5. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Výstup: 1. vůz, 4. dveře<br> 4. vůz, 3. dveře");
    }
    else if(arg1 == "Pražského Povstání")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Vyšehrad")
    {
        if(dir == "Letňany") ui->ExitList->setText("Podchod do protějšího směru: 2. vůz, 4. dveře<br> Výstup hotel Corinthia: 3. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup Vyšehrad, Kongresové centrum: 3. vůz, 1. dveře<br>Podchod do protějšího směru: 4. vůz, 1. dveře");
    }
    else if(arg1 == "I. P. Pavlova")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br>Výtah: 4. vůz, 2. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výtah: 2. vůz, 3. dveře<br>Výstup: 5. vůz, 4. dveře");
    }
    //Muzeum under line A
    else if(arg1 == "Hlavní nádraží")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výtah: 1. vůz, 1. dveře<br>Výstup: 1. vůz, 1. dveře<br>2. vůz, 1. dveře<br>4. vůz, 4. dveře<br>5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br>2. vůz, 1. dveře<br>4. vůz, 4. dveře<br>5. vůz, 4. dveře<br>Výtah: 5. vůz, 4. dveře");
    }
    //Florenc under line B
    else if(arg1 == "Vltavská")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup, výtah: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup, výtah: 1. vůz, 1. dveře");
    }
    else if(arg1 == "Nádraží Holešovice")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výtah, výstup vlak: 1. vůz, 1. dveře<br>Výstup BUS: 5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup BUS: 1. vůz, 1. dveře<br>Výtah, výstup vlak: 5. vůz, 4. dveře");
    }
    else if(arg1 == "Kobylisy")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br>5. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře<br>5. vůz, 4. dveře");
    }
    else if(arg1 == "Ládví")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výtah: 2. vůz, 3. dveře<br>Výstup: 2. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 4. vůz, 1. dveře<br>Výtah: 4. vůz, 2. dveře");
    }
    else if(arg1 == "Střížkov")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výstup: 2. vůz, 2. dveře<br>3. vůz, 2. dveře<br>Výtah: 3. vůz, 3. dveře<br>Výstup: 3. vůz, 4. dveře<br>4. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 1. vůz, 4. dveře<br>2. vůz, 1. dveře<br>Výtah: 3. vůz, 2. dveře<br>Výstup: 4.vůz, 4. dveře");
    }
    else if(arg1 == "Prosek")
    {
        if(dir == "Letňany") ui->ExitList->setText("Výtah: 2. vůz, 2. dveře<br>Výstup: 2. vůz, 3. dveře<br>4. vůz, 4. dveře");
        if(dir == "Háje") ui->ExitList->setText("Výstup: 2. vůz, 1. dveře<br>4. vůz, 2. dveře<br>Výtah: 4. vůz, 4. dveře");
    }
    else if(arg1 == "Letňany") ui->ExitList->setText("Výstup: 1. vůz, 1. dveře"); //END C
}
