# Metro

Semestrální práce PPC

Současná verze programu je první funkční a měla by představovat do určité míry Proof-of-concept. Další rozvoj práce by spočíval v tom, že by se změnil systém ukládání dat na něco méně cave-man než to natvrdo nasekat do kódu. Například ukládání do souboru databáze, nebo requestování dat z internetového serveru.

Rovněž bych chtěl přidat možnost zadat trasu systémem Z/Do s tím, že program ukáže do jakých dveří nastoupit tak, aby vyšel co nejoptimálněji i případný přestup.

Rovněž by bylo záhodno rozdělit vypisovací QLabel na dva, aby šlo formátovat výstup jako tabulku.

Do budoucna bych přidal i nějaké vizuály, jako například plány stanic.

(c) Jan Veselý MMXX